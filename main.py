from aiogram import Bot, Dispatcher, types, executor
import requests, json, bs4, lxml, markups, urlextract, asyncio, datetime
from apscheduler.schedulers.asyncio import AsyncIOScheduler

BAD_STATUSES = [404, 405, 406, 409, 410, 411, 412, 413, 414, 415, 416, 417]

with open('data.json', 'r') as f:
    data = json.load(f)
    f.close()

interval = data['interval'] if 'interval' in data else 1
password = data['password'] if 'password' in data else 'ПАРОЛЬ'
TOKEN = data['TOKEN'] if 'TOKEN' in data else ''

bot = Bot(token=TOKEN)
dp = Dispatcher(bot)
scheduler = AsyncIOScheduler(timezone='Europe/Moscow')
main_job = None
extractor = urlextract.URLExtract()

@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    await bot.delete_message(message.chat.id, message.message_id)
    await bot.send_message(message.chat.id,
                           'Привет, я бот который поможет следить за состоянием сайтов!\nЧтоб использовать меня, необходимо авторизоваться.\nДля авторизации введите:\n/pass password\nгде password - пароль.')

@dp.message_handler(commands=['pass'])
async def auth(message: types.Message):
    await bot.delete_message(message.chat.id, message.message_id-1)
    await bot.delete_message(message.chat.id, message.message_id)
    if message.text[6:] == data['password']:
        data['chats'].append(message.from_user.id) if message.from_user.id not in data['chats'] else None
        with open('data.json', 'w') as f:
            json.dump(data, f, indent=4)
            f.close()
        await bot.send_message(message.chat.id, 'Вы успешно авторизованы!\nТеперь я обязательно сообщу, если какой-то сайт из списка отслеживаемых будет работать некорректно!', reply_markup=markups.MainMenu)
    else:
        await bot.send_message(message.chat.id, 'Неверный пароль!')

@dp.callback_query_handler(text="btnListURL")
async def send_URL_list_btn(message: types.Message):
    await bot.delete_message(message.from_user.id, message.message.message_id)
    try:
        with open('data.json', 'r') as f:
            data = json.load(f)
            f.close()
        if not message.from_user.id in data['chats']:
            raise Exception('Вы не авторизованы!')
        URLS = '----------\n\n'
        for i in data['URLS']:
            URLS+=i+"\n\n"
        URLS+="----------\n"
        await bot.send_message(message.from_user.id, f'Список отслеживаемых сайтов:\n{URLS}', reply_markup=markups.MainMenu)
    except Exception as ex:
        await bot.send_message(message.from_user.id, f'При получении списка отслеживаемых сайтов произошла ошибка:\n{ex}', reply_markup=markups.MainMenu)

@dp.callback_query_handler(text="btnAddURL")
async def send_add_URL_btn(message: types.Message):
    await bot.delete_message(message.from_user.id, message.message.message_id)
    if not message.from_user.id in data['chats']:
        await bot.send_message(message.chat.id, 'Вы не авторизованы!')
    else:
        await bot.send_message(message.from_user.id, 'Для добавления сайта в список отслеживаемых введите:\n/add url\nГде url - ссылка на сайт который необходимо добавить в список.', reply_markup=markups.MainMenu)

@dp.callback_query_handler(text="btnDelURL")
async def send_add_URL_btn(message: types.Message):
    await bot.delete_message(message.from_user.id, message.message.message_id)
    if not message.from_user.id in data['chats']:
        await bot.send_message(message.chat.id, 'Вы не авторизованы!')
    else:
        await bot.send_message(message.from_user.id, 'Для удаления сайта из списка отслеживаемых введите:\n/del url\nГде url - ссылка на сайт который необходимо удалить из списка.', reply_markup=markups.MainMenu)

@dp.callback_query_handler(text="btnChangeInterval")
async def send_add_URL_btn(message: types.Message):
    await bot.delete_message(message.from_user.id, message.message.message_id)
    if not message.from_user.id in data['chats']:
        await bot.send_message(message.chat.id, 'Вы не авторизованы!')
    else:
        await bot.send_message(message.from_user.id, f'Текущий интервал проверки - {interval} минут.\nДля изменения интервала задержки введите:\n/cci time\nГде time - новый интервал проверки в минутах.', reply_markup=markups.MainMenu)

@dp.message_handler(commands=['cci'])
async def change_interval(message: types.Message):
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.delete_message(message.from_user.id, message.message_id)

    try:
        with open('data.json', 'r') as f:
            data = json.load(f)
            f.close()
        if not message.from_user.id in data['chats']:
            raise Exception('Вы не авторизованы!')
        time = message.text[5:]
        if not time.isdigit():
            raise Exception('Неверный формат введенного времени')
        else:
            interval = int(time)
            
            with open('data.json', 'r') as f:
                data = json.load(f)
                f.close()
            
            data['interval'] = interval

            with open('data.json', 'w') as f:
                json.dump(data, f)
                f.close()
            
            scheduler.remove_all_jobs()
            scheduler.add_job(henchman, trigger='interval', minutes=data['interval'])
            
            await bot.send_message(message.from_user.id, f'Интервал проверки изменен на {interval} минут.', reply_markup=markups.MainMenu)

    except Exception as ex:
        await bot.send_message(message.from_user.id, f'При изменении интервала произошла ошибка:\n\n{ex}', reply_markup=markups.MainMenu)
        return

@dp.message_handler(commands=['add'])
async def add_URL(message: types.Message):
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.delete_message(message.from_user.id, message.message_id)

    try:
        with open('data.json', 'r') as f:
            data = json.load(f)
            f.close()
        if not message.from_user.id in data['chats']:
            raise Exception('Вы не авторизованы!')
        
        url = message.text[5:]

        if await check_url(url)==False:
            raise Exception('Не найден сайт по данной ссылке')
        
        with open("data.json", "r") as f:
            data = json.load(f)
            f.close()
        
        data['URLS'].append(url)

        with open("data.json", "w") as f:
            json.dump(data, f)
            f.close()
        
        await bot.send_message(message.from_user.id, f'Сайт {url}\nуспешно добавлени в список!', reply_markup=markups.MainMenu)

    except Exception as ex:
        await bot.send_message(message.from_user.id, f'При добавлении сайта {url} в список произошла ошибка:\n\n{ex}', reply_markup=markups.MainMenu)

@dp.message_handler(commands=['del'])
async def del_URL(message: types.Message):
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.delete_message(message.from_user.id, message.message_id)

    try:
        with open('data.json', 'r') as f:
            data = json.load(f)
            f.close()
        if not message.from_user.id in data['chats']:
            raise Exception('Вы не авторизованы!')
        
        url = message.text[5:]

        with open("data.json", "r") as f:
            data = json.load(f)
            f.close()
        
        if url in data['URLS']:
            data['URLS'].remove(url)

            with open("data.json", "w") as f:
                json.dump(data, f)
                f.close()

            await bot.send_message(message.from_user.id, f'Сайт {url} успешно удален!', reply_markup=markups.MainMenu)

    except Exception as ex:
        await bot.send_message(message.from_user.id, f'При удалени сайта {url} произошла ошибка:\n\n{ex}', reply_markup=markups.MainMenu)

async def check_url(url: str):
    try:
        r = requests.get(url)
        soup = bs4.BeautifulSoup(r.text, 'lxml')
        try:
            body = soup('body')
            return not (500<=r.status_code<=599\
                        or r.status_code in BAD_STATUSES)
        except Exception as ex:
            return False
    except Exception as ex:
        return False

async def henchman():

    with open('data.json', 'r') as f:
        data = json.load(f)
        f.close()
    for url in data['URLS']:
        if not await check_url(url)==True:
            for chat in data['chats']:
                await bot.send_message(chat, f'Сайт {url} не отвечает', reply_markup=markups.MainMenu)
        else:
            print(f'{datetime.datetime.now()} - все сайты работают коррестно')    

async def start():
    scheduler.add_job(henchman, trigger='interval', minutes=data['interval'])
    scheduler.start()
    await dp.start_polling()

if __name__ == '__main__':
    asyncio.run(start())

