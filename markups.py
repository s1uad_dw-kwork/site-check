from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

MainMenu = InlineKeyboardMarkup(row_width=1)
btnAddURl = InlineKeyboardButton(text="Добавить сайт", callback_data="btnAddURL")
btnDelURL = InlineKeyboardButton(text="Удалить сайт", callback_data="btnDelURL")
btnListURL = InlineKeyboardButton(text="Cписок отслеживаемых сайтов", callback_data="btnListURL")
btnChangeInterval = InlineKeyboardButton(text="Изменить интервал проверки", callback_data="btnChangeInterval")

MainMenu.insert(btnChangeInterval)
MainMenu.insert(btnListURL)
MainMenu.insert(btnAddURl)
MainMenu.insert(btnDelURL)